//
//  ViewController.swift
//  testswift
//
//  Created by Mahindra on 19/04/20.
//  Copyright © 2020 Mahindra. All rights reserved.
//

import UIKit
import BluesenseFramework
class ViewController: UIViewController {
    
    var  connect   :Bool = false

    @IBAction func Audio(_ sender: Any) {
        presentaudioStatusView()
    }
    @IBAction func Climate(_ sender: Any) {
           presentclimateStatusView()
       }
    
    @IBAction func Ecosense(_ sender: Any)
    {
        presentEcosenseStatusView()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if BluesenseFramework.BTConnectionController.connectToInfotainment(){
             connect = true
        }
        else{
            connect = false
        }
    
    }

    func presentaudioStatusView() {
        if (!connect){
            let modalView = BluesenseFramework.AudioController(frame: self.view.frame)
                     view.addSubview(modalView)
        }
                else {
                    let alertController = UIAlertController (title: "Mahindra Bluesense", message: "Turn on Bluetooth and Pair the device with MAHINDRA", preferredStyle: .alert)
        
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
        
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)") // Prints true
                            })
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
        
                    present(alertController, animated: true, completion: nil)
        
        
                }
       
      }
    func presentclimateStatusView() {
        if (!connect){
        let modalView1 =  BluesenseFramework.ClimateControl(frame: self.view.frame)
          view.addSubview(modalView1)
        }
                else {
                    let alertController = UIAlertController (title: "Mahindra Bluesense", message: "Turn on Bluetooth and Pairthedevice with MAHINDRA", preferredStyle: .alert)
        
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
        
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)") // Prints true
                            })
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
        
                    present(alertController, animated: true, completion: nil)
        
        
               }
    }
    func presentEcosenseStatusView() {
         if (!connect){
        let modalView1 =  BluesenseFramework.EcosenseSummary(frame: self.view.bounds)
          view.addSubview(modalView1)
    
    }else {
                let alertController = UIAlertController (title: "Mahindra Bluesense", message: "Turn on Bluetooth and Pair the device with MAHINDRA", preferredStyle: .alert)
    
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
    
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertController.addAction(cancelAction)
    
                present(alertController, animated: true, completion: nil)
    
    
           }
    }
    
    
}


